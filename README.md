# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Coursework 97 [Team Research and Development Project]
* (https://tayyab512@bitbucket.org/tayyab512/coursework-97.git)

### Group Members [Coursework 97] ###

* Tayyab Hussain [18054981] Repo owner or admin
* Umut Naderi [18046294] Repo admin
* Rohith Kumar Mamindla [18050610]
* Venkata RajaShaker Varma Jampani [18018021]
* Sarabjeet Singh [18020629]

### Files in this repository ###

* TRDP Coursework Portfolio.PDF  						* Coursework Report
* Coursework Dataset.SAV 								* Dataset of London Borough		
* SPSS Results.SPV 										* Results of export file from SPSS
* SPSS Results\ 										* Folder for the SPSS Results, Contains Images
	* Correlation.PNG
	* Frequency Stats EmploymentRate.PNG
	* Frequency Stats Female Employment Rate.PNG
	* Frequency Stats Gross Income Female.PNG
	* Frequency Stats Gross Income Male.PNG
	* Frequency Stats LifeSatisfaction.PNG 
	* Frequency Stats Male Employment Rate.PNG
	* Descriptive Stats EmploymentRate.PNG
	* Descriptive Stats LifeSatisfaction.PNG
	* Graph Bar Plot.PNG
	* Graph Box Plot.PNG
	* Graph Histogram.PNG
	* Graph Scatterplot Coorelation.PNG
	* Graph Scatterplot.PNG
	* Non Parametric Correlation.PNG
	* T-Test Significance.PNG
	